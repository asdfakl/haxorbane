package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"path/filepath"
	"sync"

	"gitlab.com/asdfakl/haxorbane/configuration"
	"gitlab.com/asdfakl/haxorbane/db"
	"gitlab.com/asdfakl/haxorbane/jail"
	"gitlab.com/asdfakl/haxorbane/log"
	"golang.org/x/sys/unix"
)

var Version string = "undefined"

func must(msg string, err error) {
	if err != nil {
		die(msg, err)
	}
}

func die(msg string, err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "fatal: %s: %v\n", msg, err)
	}
	os.Exit(1)
}

func main() {
	var (
		printVersion              = false
		printExampleConfiguration = false
		cfgFile                   = ""
	)

	flag.BoolVar(&printVersion, "version", false, "print version to stdout and exit")
	flag.BoolVar(&printExampleConfiguration, "example-config", false, "print example configuration to stdout and exit")
	flag.StringVar(&cfgFile, "c", "/etc/haxorbane/config.toml", "load configuration from this path")
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: %s [OPTIONS]\n\n", os.Args[0])
		flag.PrintDefaults()
	}
	flag.Parse()

	if printVersion {
		fmt.Println(Version)
		return
	}
	if printExampleConfiguration {
		fmt.Println(configuration.Example)
		return
	}

	abs, err := filepath.Abs(cfgFile)
	must("Invalid filepath on config", err)

	cfg, err := configuration.Load(abs)
	must("Can not load configurations", err)

	db.Init(cfg.SQLite3File)

	must("Can not Init log", log.Init(cfg.LogFile, cfg.LogColors, cfg.LogLevel))

	log.System.Printf("version %s", Version)

	log.System.Println("configuration loaded from", abs)

	errc := make(chan jail.Error)
	sigc := make(chan os.Signal, 3)
	jails := make(map[string]*jail.Jail)
	banMutex := &sync.Mutex{}

	for name, jailCfg := range cfg.Jails {
		log.System.Printf("starting jail %s, source %s\n", name, jailCfg.Source)

		jails[name] = jail.New(name, cfg, jailCfg, cfg.BanAction, banMutex)
		jails[name].Start(errc)
	}

	signal.Notify(sigc, unix.SIGINT, unix.SIGTERM, unix.SIGHUP)

	for {
		select {
		case jailErr := <-errc:
			if jailErr.WasClean() {
				log.System.Printf("jail %s graceful shutdown", jailErr.Name)
			} else {
				log.Error.Printf("jail %s abnormal exit: %s", jailErr.Name, jailErr.Error())
			}
			delete(jails, jailErr.Name)

			if len(jails) == 0 {
				log.System.Println("no jails running, exiting")
				return
			}
		case sig := <-sigc:
			log.System.Printf("got signal: %v", sig)

			log.System.Println("stopping all jails")

			for _, j := range jails {
				j.Stop()
			}
		}
	}
}
