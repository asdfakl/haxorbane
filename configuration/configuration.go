package configuration

import (
	"fmt"
	"io/ioutil"
	"net"
	"regexp"
	"strings"

	"github.com/burntsushi/toml"
)

var defaultIPRE = regexp.MustCompile(`(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})`)

type Configuration struct {
	LogColors     bool     `toml:"log_colors"`
	LogFile       string   `toml:"log_file"`
	LogLevel      string   `toml:"log_level"`
	SQLite3File   string   `toml:"sqlite3_file"`
	WhitelistCIDR []string `toml:"whitelist"`

	BanAction []string `toml:"ban_action"`

	Jails map[string]*JailConfiguration `toml:"jails"`

	Whitelist []*net.IPNet
}

func Load(fname string) (*Configuration, error) {
	b, err := ioutil.ReadFile(fname)
	if err != nil {
		return nil, err
	}

	cfg := defaultConfiguration()
	if _, err = toml.Decode(string(b), cfg); err != nil {
		return nil, err
	}

	if err = cfg.validate(); err != nil {
		return nil, fmt.Errorf("configuration validation error: %w", err)
	}

	return cfg, nil
}

func defaultConfiguration() *Configuration {
	return &Configuration{
		LogFile: "/var/log/haxorbane/haxorbane.log",
		Jails:   make(map[string]*JailConfiguration),
	}
}

func (cfg *Configuration) validate() error {
	cfg.LogFile = strings.TrimSpace(cfg.LogFile)
	cfg.SQLite3File = strings.TrimSpace(cfg.SQLite3File)

	if cfg.SQLite3File == "" {
		return fmt.Errorf("sqlite3_file empty or undefined")
	}

	for k, j := range cfg.Jails {
		if !j.Enabled {
			delete(cfg.Jails, k)
		}
	}

	if len(cfg.Jails) == 0 {
		return fmt.Errorf("jails must not be empty")
	}

	if len(cfg.BanAction) == 0 {
		return fmt.Errorf("ban_action must not be empty")
	}

	for _, candidate := range cfg.WhitelistCIDR {
		_, cidr, err := net.ParseCIDR(candidate)
		if err != nil {
			return fmt.Errorf("parse whitelist cidr block '%s': %w", candidate, err)
		}
		cfg.Whitelist = append(cfg.Whitelist, cidr)
	}

	for name, jail := range cfg.Jails {
		if err := jail.validate(); err != nil {
			return fmt.Errorf("jail %s configuration validation error: %w", name, err)
		}
	}

	return nil
}
