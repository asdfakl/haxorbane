package configuration

const Example = `log_colors   = false
log_file     = "/var/log/haxorbane.log"
log_level    = "INFO"
sqlite3_file = "/var/lib/haxorbane/haxorbane.db"

ban_action = [
	"ipset",
	"add",
	"haxorbane",
	"${ip}",
	"-exist",
]

whitelist = [
	"84.250.0.0/16",
]

[jails.nginx]
enabled     = true
source      = "/var/log/nginx/access.log"
time        = '\[(\d+/\w+/\d{4}:\d{2}:\d{2}:\d{2}\s\+\d{4})\]'
time_format = '2/Jan/2006:15:04:05 +0000'

	[[jails.nginx.rules]]
	enabled         = true
	name            = 'evil_bufferoverflow_haxor'
	filter          = '(\\x[0-9A-F]{2}.*){3,}?'
	time_window     = '5m'
	ban_duration    = '24h'

	[[jails.nginx.rules]]
	enabled         = true
	name            = 'php_vulnerability_scan'
	filter          = '\.php'
	allowed_strikes = 2
	time_window     = '10m'
	ban_duration    = '1h'

	[[jails.nginx.rules]]
	enabled         = false
	name            = 'dos_1000_30m'
	allowed_strikes = 1000
	time_window     = '30m'

[jails.ssh]
enabled     = true
source      = "/var/log/auth.log"
time        = '^(\w+\s+\d+ \d{2}:\d{2}:\d{2} )'
time_format = 'Jan 2 15:04:05'

	[[jails.ssh.rules]]
	enabled         = true
	name            = 'ssh_bruteforce'
	filter          = 'Invalid user'
	allowed_strikes = 3
	time_window     = '15m'
	ban_duration    = '168h'`
