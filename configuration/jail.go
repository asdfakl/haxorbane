package configuration

import (
	"fmt"
	"regexp"
	"strings"
	"time"
)

var ruleNameRE = regexp.MustCompile(`^[A-Za-z0-9-_]+$`)

type JailConfiguration struct {
	Enabled        bool                 `toml:"enabled"`
	Source         string               `toml:"source"`
	TimeExpression string               `toml:"time"`
	IPExpression   string               `toml:"ip"`
	TimeFormat     string               `toml:"time_format"`
	Rules          []*RuleConfiguration `toml:"rules"`

	TimeRE *regexp.Regexp `toml:"-"`
	IPRE   *regexp.Regexp `toml:"-"`
}

type RuleConfiguration struct {
	Enabled               bool   `toml:"enabled"`
	Name                  string `toml:"name"`
	FilterExpression      string `toml:"filter"`
	AllowedStrikes        uint   `toml:"allowed_strikes"`
	TimeWindowExpression  string `toml:"time_window"`
	BanDurationExpression string `toml:"ban_duration"`

	BanDuration time.Duration  `toml:"-"`
	TimeWindow  time.Duration  `toml:"-"`
	FilterRE    *regexp.Regexp `toml:"-"`
}

func (jail *JailConfiguration) RuleByName(name string) *RuleConfiguration {
	for _, rule := range jail.Rules {
		if rule.Name == name {
			return rule
		}
	}

	panic(fmt.Errorf("jail rule not found by name '%s'", name))
}

func (jail *JailConfiguration) validate() error {
	jail.Source = strings.TrimSpace(jail.Source)
	if jail.Source == "" {
		return fmt.Errorf("empty source")
	}

	jail.TimeFormat = strings.TrimSpace(jail.TimeFormat)
	if jail.TimeFormat == "" {
		return fmt.Errorf("empty time format")
	}

	if jail.TimeExpression == "" {
		return fmt.Errorf("empty time expression")
	}
	timeRE, err := regexp.Compile(jail.TimeExpression)
	if err != nil {
		return fmt.Errorf("invalid time expression: %w", err)
	}
	jail.TimeRE = timeRE

	if jail.IPExpression == "" {
		jail.IPRE = defaultIPRE
	} else {
		ipRE, err := regexp.Compile(jail.IPExpression)
		if err != nil {
			return fmt.Errorf("invalid ip expression: %w", err)
		}
		jail.IPRE = ipRE
	}

	enabled := []*RuleConfiguration{}
	for _, rule := range jail.Rules {
		if rule.Enabled {
			enabled = append(enabled, rule)
		}
	}
	jail.Rules = enabled

	if len(jail.Rules) == 0 {
		return fmt.Errorf("rules must not be empty")
	}

	names := make(map[string]struct{})
	for i, rule := range jail.Rules {
		if err = rule.validate(); err != nil {
			return fmt.Errorf("rule %d validation error: %w", i, err)
		}
		if _, ok := names[rule.Name]; ok {
			return fmt.Errorf("duplicate rule name: %s", rule.Name)
		}
		names[rule.Name] = struct{}{}
	}

	return nil
}

func (rule *RuleConfiguration) validate() error {
	if !ruleNameRE.MatchString(rule.Name) {
		return fmt.Errorf("invalid rule name: '%s'", rule.Name)
	}

	if rule.FilterExpression != "" {
		filterRE, err := regexp.Compile(rule.FilterExpression)
		if err != nil {
			return fmt.Errorf("invalid filter expression: %w", err)
		}
		rule.FilterRE = filterRE
	}

	if rule.TimeWindowExpression == "" {
		return fmt.Errorf("empty time window")
	}
	window, err := time.ParseDuration(rule.TimeWindowExpression)
	if err != nil {
		return fmt.Errorf("invalid time window: %w", err)
	}
	if window < time.Minute {
		return fmt.Errorf("time window must be at least one minute")
	}
	rule.TimeWindow = window

	if rule.BanDurationExpression == "" {
		return fmt.Errorf("empty ban duration")
	}
	banDuration, err := time.ParseDuration(rule.BanDurationExpression)
	if err != nil {
		return fmt.Errorf("invalid ban duration: %w", err)
	}
	if banDuration < time.Minute {
		return fmt.Errorf("ban duration must be at least one minute")
	}
	rule.BanDuration = banDuration

	return nil
}
