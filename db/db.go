package db

import (
	"database/sql"
	"fmt"
	"net"
	"os"

	_ "github.com/mattn/go-sqlite3"
)

var database *sql.DB
var addBanned *sql.Stmt
var updateBanned *sql.Stmt
var addEvents *sql.Stmt

type Ban struct {
	IP    string
	Until int64
}

func check(msg string, err error) {
	if err != nil {
		exit(msg, err)
	}
}

func exit(msg string, err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %v\n", msg, err)
	}
	os.Exit(1)
}

func Init(databaseFile string) {

	check("Failed to open or Create Database", openDb(databaseFile))
	check("Failed to open table Banned", openTableBanned())
	check("Failed to open table Events", openTableEvents())
	check("Failed to initialize statements", initStatements())
}

func openDb(databaseFile string) error {
	db, err := sql.Open("sqlite3", databaseFile)
	if err != nil {
		return err
	}
	database = db
	return nil
}

func openTableBanned() error {
	_, err := database.Exec("CREATE TABLE IF NOT EXISTS banned (id INTEGER PRIMARY KEY, ip_address TEXT UNIQUE, until INTEGER)")
	return err
}

func openTableEvents() error {
	_, err := database.Exec("CREATE TABLE IF NOT EXISTS events (id INTEGER PRIMARY KEY, ip_address TEXT, rule TEXT, event_time INTEGER, jail TEXT)")
	return err
}

func initStatements() error {
	stmtAddBanned, err := database.Prepare("INSERT INTO banned (ip_address, until) VALUES (?, ?)")
	if err != nil {
		return err
	}
	addBanned = stmtAddBanned

	stmtAddEvents, err := database.Prepare("INSERT INTO events(ip_address, rule, event_time, jail) VALUES (?,?,?,?)")
	if err != nil {
		return err
	}
	addEvents = stmtAddEvents

	stmtUpdateBanned, err := database.Prepare("UPDATE banned SET until = ? WHERE ip_address = ?")
	if err != nil {
		return err
	}
	updateBanned = stmtUpdateBanned

	return nil
}

func AddToBanned(ipAddress string, until int64) error {

	_, err := addBanned.Exec(ipAddress, until)
	return err
}

func UpdateBanned(ipAddress string, until int64) error {
	_, err := updateBanned.Exec(until, ipAddress)
	return err
}

func AddToEvents(ipAddress net.IP, rule string, time int64, jail string) error {

	_, err := addEvents.Exec(ipAddress.String(), rule, time, jail)
	return err
}

func RemoveRowFromBanned(id int) error {

	_, err := database.Exec("DELETE FROM banned WHERE id = ?", id)
	return err
}

func GetLastEventTimestampsByJail(jail string) map[string]int64 {
	rows, err := database.Query(`
		SELECT rule, MAX(event_time)
		FROM events
		WHERE jail = ?
		GROUP BY rule
	`, jail)
	if err != nil {
		panic(fmt.Errorf("get last event timestamps by jail: %v", err))
	}
	defer rows.Close()

	m := make(map[string]int64)
	for rows.Next() {
		rule, timestamp := "", int64(0)
		if err = rows.Scan(&rule, &timestamp); err != nil {
			panic(fmt.Errorf("get last event timestamps by jail: %v", err))
		}
		m[rule] = timestamp
	}

	if err = rows.Err(); err != nil {
		panic(fmt.Errorf("get last event timestamps by jail: %v", err))
	}

	return m
}

func GetEventCount(from, to int64, jail, rule string, ip net.IP) uint {
	count := uint(0)

	if err := database.QueryRow(`
		SELECT COUNT(*)
		FROM events
		WHERE event_time >= ? AND event_time <= ? AND jail = ? AND rule = ? AND ip_address = ?
	`, from, to, jail, rule, ip.String()).Scan(&count); err != nil {
		panic(fmt.Errorf("get event count: %v", err))
	}

	return count
}

func GetBanByIP(ip string) *Ban {
	ban := &Ban{}

	if err := database.QueryRow(`
		SELECT ip_address, until
		FROM banned
		WHERE ip_address = ?
	`, ip).Scan(&ban.IP, &ban.Until); err != nil {
		if err == sql.ErrNoRows {
			return nil
		}
		panic(fmt.Errorf("get ban by ip: %v", err))
	}

	return ban
}
