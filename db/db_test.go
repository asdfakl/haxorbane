package db

import (
	"net"
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	Init()
	os.Exit(m.Run())
}

func TestAddToBanned(t *testing.T) {
	ipaddress := "000.240.382.43"
	banduration := 31556952
	err := AddToBanned(ipaddress, banduration)
	if err != nil {
		t.Error(err)
	}
}
func TestAddToEvents(t *testing.T) {
	ipaddress := net.ParseIP("295.153.131.147")
	rule := "evil_bufferoverflow_haxor"
	time := int64(1583528829)
	jail := "nginx event"
	err := AddToEvents(ipaddress, rule, time, jail)
	if err != nil {
		t.Error(err)
	}
}
func TestRemoveRowFromBanned(t *testing.T) {
	id := 3
	err := RemoveRowFromBanned(id)
	if err != nil {
		t.Error(err)
	}
}
