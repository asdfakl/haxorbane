module gitlab.com/asdfakl/haxorbane

go 1.13

require (
	github.com/burntsushi/toml v0.3.1
	github.com/fsnotify/fsnotify v1.4.7
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	golang.org/x/sys v0.0.0-20200219091948-cb0a6d8edb6c
)
