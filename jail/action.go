package jail

import (
	"bytes"
	"context"
	"fmt"
	"os/exec"
	"strings"
	"unicode/utf8"

	"gitlab.com/asdfakl/haxorbane/log"
)

const (
	parserModeRegular ParserMode = 1 + iota
	parserModeExpectInterpolation
	parserModeInterpolation
)

type ParserMode int

func executeAction(action []string, env map[string]string) error {
	interpolated := make([]string, len(action))

	for i, part := range action {
		interpolation, err := interpolateVariables(part, env)
		if err != nil {
			return fmt.Errorf("could not interpolate action part %d: %v", i, err)
		}
		interpolated[i] = interpolation
	}

	ctx, cancel := context.WithTimeout(context.Background(), ActionExecutionTimeout)
	defer cancel()

	cmd := exec.CommandContext(ctx, interpolated[0], interpolated[1:]...)

	for k, v := range env {
		cmd.Env = append(cmd.Env, k+"="+v)
	}

	stdout := &bytes.Buffer{}
	stderr := &bytes.Buffer{}

	cmd.Stdout = stdout
	cmd.Stderr = stderr

	log.Debug.Printf(
		"execute action: action = [ %s ], env = [ %s ]",
		strings.Join(interpolated, " "),
		strings.Join(cmd.Env, " "),
	)

	err := cmd.Run()

	if l := stdout.Len(); l != 0 {
		log.Info.Printf("action stdout:\n%s", stdout.String())
	}
	if stderr.Len() != 0 {
		log.Warn.Printf("action stderr:\n%s", stderr.String())
	}

	return err
}

func interpolateVariables(s string, variables map[string]string) (string, error) {
	if !utf8.ValidString(s) {
		return "", fmt.Errorf("invalid utf8 sequence")
	}

	b, interpolation := strings.Builder{}, strings.Builder{}

	mode, escape := parserModeRegular, false

	for i, r := range s {
		switch mode {
		case parserModeRegular:
			if escape {
				escape = false
				b.WriteRune(r)
				break
			}

			switch r {
			case '\\':
				escape = true
			case '$':
				mode = parserModeExpectInterpolation
			default:
				b.WriteRune(r)
			}
		case parserModeExpectInterpolation:
			if r != '{' {
				return "", fmt.Errorf("invalid interpolation expression at byte offset %d, expect '{' after '$'", i)
			}
			mode = parserModeInterpolation
		case parserModeInterpolation:
			if escape {
				escape = false
				interpolation.WriteRune(r)
			} else {
				switch r {
				case '\\':
					escape = true
				case '}':
					k := interpolation.String()
					value, ok := variables[strings.ToUpper(k)]
					if !ok {
						return "", fmt.Errorf("unknown variable '%s' at byte offset %d", k, i)
					}

					b.WriteString(value)
					mode = parserModeRegular
					interpolation.Reset()
				default:
					interpolation.WriteRune(r)
				}
			}
		default:
			panic(fmt.Errorf("illegal parser mode: %v", mode))
		}
	}

	switch mode {
	case parserModeExpectInterpolation:
		return "", fmt.Errorf("expect interpolation expression at trailing '$', tip: escape with '\\'")
	case parserModeInterpolation:
		return "", fmt.Errorf("unterminated interpolation expression: '%s', tip: terminate with '}'", interpolation.String())
	}

	if escape {
		return "", fmt.Errorf("trailing escape character '\\', tip: add another '\\' if the string must end with a backslash")
	}

	return b.String(), nil
}
