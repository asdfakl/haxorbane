package jail

type Error struct {
	Name string
	err  error
}

func (e *Error) WasClean() bool {
	return e.err == nil
}

func (e *Error) Error() string {
	return e.err.Error()
}
