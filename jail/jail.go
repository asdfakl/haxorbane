package jail

import (
	"os"
	"strconv"
	"sync"
	"time"

	"gitlab.com/asdfakl/haxorbane/configuration"
	"gitlab.com/asdfakl/haxorbane/db"
	"gitlab.com/asdfakl/haxorbane/log"
	"gitlab.com/asdfakl/haxorbane/watcher"
)

const DatetimeFormat = time.RFC3339

const ActionExecutionTimeout time.Duration = 5 * time.Second

var envPath = ""

type Jail struct {
	name      string
	commonCfg *configuration.Configuration
	cfg       *configuration.JailConfiguration
	banAction []string
	banMutex  *sync.Mutex

	once  sync.Once
	stopc chan struct{}
}

func init() {
	envPath = os.Getenv("PATH")
}

func New(name string, commonCfg *configuration.Configuration, jailCfg *configuration.JailConfiguration, banAction []string, banMutex *sync.Mutex) *Jail {
	return &Jail{
		name:      name,
		commonCfg: commonCfg,
		cfg:       jailCfg,
		banAction: banAction,
		banMutex:  banMutex,

		stopc: make(chan struct{}),
	}
}

func (j *Jail) Start(errc chan<- Error) {
	j.once.Do(func() {
		go j.run(errc)
	})
}

func (j *Jail) Stop() {
	select {
	case j.stopc <- struct{}{}:
	default:
	}
}

func (j *Jail) run(errc chan<- Error) {
	jerr := Error{Name: j.name}
	defer func() {
		errc <- jerr
	}()

	w := watcher.NewINotify(j.cfg)
	eventc := make(chan *watcher.Event, 10)
	w.Start(eventc)
	defer w.Stop()

	lastEventTS := db.GetLastEventTimestampsByJail(j.name)

run_loop:
	for {
		select {
		case <-j.stopc:
			w.Stop()
		case event, ok := <-eventc:
			if !ok {
				log.System.Printf("jail %s watcher stopped", j.name)
				return
			}

			if event.Error != nil {
				if jerr.err == nil {
					jerr.err = event.Error
				}
				w.Stop()
				continue run_loop
			}

			// incorrect temporal order
			if event.Time.Unix() < lastEventTS[event.Rule] {
				continue run_loop
			}
			lastEventTS[event.Rule] = event.Time.Unix()

			log.Debug.Printf("jail %s event: rule = %s, ip = %v, time = %s", j.name, event.Rule, event.IP, event.Time.Format(DatetimeFormat))
			if err := db.AddToEvents(event.IP, event.Rule, event.Time.Unix(), j.name); err != nil {
				log.Error.Printf("insert event: %v", err)
				continue run_loop
			}

			rule := j.cfg.RuleByName(event.Rule)

			for _, cidr := range j.commonCfg.Whitelist {
				if cidr.Contains(event.IP) {
					log.Warn.Printf(
						"jail %s event from whitelisted cidr: rule = %s, ip = %v, time = %s, whitelist = %v",
						j.name,
						rule.Name,
						event.IP,
						event.Time.Format(DatetimeFormat),
						cidr,
					)
					continue run_loop
				}
			}

			if strikes := db.GetEventCount(event.Time.Add(-rule.TimeWindow).Unix(), event.Time.Unix(), j.name, rule.Name, event.IP); strikes > rule.AllowedStrikes {
				j.updateBanStatus(event, rule, strikes)
			}
		}
	}
}

func (j *Jail) updateBanStatus(event *watcher.Event, rule *configuration.RuleConfiguration, strikes uint) {
	until := event.Time.Add(rule.BanDuration)

	if !until.After(time.Now()) {
		log.Debug.Printf(
			"jail %s no action: rule = %s, ip = %v, strikes = %d, until = %s (ban duration already expired)",
			j.name,
			rule.Name,
			event.IP,
			strikes,
			until.Format(DatetimeFormat),
		)
		return
	}

	// handle only one ban/unban concurrently
	j.banMutex.Lock()
	defer j.banMutex.Unlock()

	ban := db.GetBanByIP(event.IP.String())

	if ban == nil {
		log.Info.Printf(
			"jail %s new ban: rule = %s, ip = %v, strikes = %d, until = %s",
			j.name,
			rule.Name,
			event.IP,
			strikes,
			until.Format(DatetimeFormat),
		)

		if err := executeAction(j.banAction, map[string]string{
			"EVENT_TIME": strconv.FormatInt(event.Time.Unix(), 10),
			"IP":         event.IP.String(),
			"JAIL":       j.name,
			"PATH":       envPath,
			"RULE":       rule.Name,
			"UNTIL":      strconv.FormatInt(until.Unix(), 10),
		}); err != nil {
			log.Error.Printf("execute ban action: %v", err)
			return
		}

		if err := db.AddToBanned(event.IP.String(), until.Unix()); err != nil {
			log.Error.Printf("insert ban: %v", err)
			return
		}
	} else if ban.Until < until.Unix() {
		log.Info.Printf(
			"jail %s update ban: rule = %s ip = %v, strikes = %d, until = %s",
			j.name,
			rule.Name,
			event.IP,
			strikes,
			until.Format(DatetimeFormat),
		)

		if err := db.UpdateBanned(event.IP.String(), until.Unix()); err != nil {
			log.Error.Printf("update ban: %v", err)
			return
		}
	} else {
		log.Debug.Printf(
			"jail %s no action: rule = %s ip = %v, strikes = %d, until = %s (already banned until %s)",
			j.name,
			rule.Name,
			event.IP,
			strikes,
			until.Format(DatetimeFormat),
			time.Unix(ban.Until, 0).Format(DatetimeFormat),
		)
	}
}
