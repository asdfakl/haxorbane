package log

import (
	"fmt"
	"io"
	stdlog "log"
	"os"
	"time"
)

const (
	LEVEL_ERROR = 1 + iota
	LEVEL_WARN
	LEVEL_INFO
	LEVEL_DEBUG
)

var Debug *stdlog.Logger
var Info *stdlog.Logger
var Warn *stdlog.Logger
var Error *stdlog.Logger
var System *stdlog.Logger

var fh io.Writer = os.Stderr

var logLevel uint

func Init(fname string, colors bool, level string) error {
	var err error

	switch level {
	case "DEBUG":
		logLevel = LEVEL_DEBUG
	case "INFO":
		logLevel = LEVEL_INFO
	case "WARN":
		logLevel = LEVEL_WARN
	case "ERROR":
		logLevel = LEVEL_ERROR
	default:
		return fmt.Errorf("unsupported log level '%s', supported levels are DEBUG, INFO, WARN and ERROR", level)
	}

	if fname != "" {
		fh, err = os.OpenFile(fname, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
		if err != nil {
			return err
		}
	}

	if colors {
		Debug = stdlog.New(&logWriter{prefix: []byte("\x1b[00;36mDEB\x1b[00m "), level: LEVEL_DEBUG}, "", stdlog.Lshortfile)
		Info = stdlog.New(&logWriter{prefix: []byte("\x1b[00;34mINF\x1b[00m "), level: LEVEL_INFO}, "", stdlog.Lshortfile)
		Warn = stdlog.New(&logWriter{prefix: []byte("\x1b[01;33mWAR\x1b[00m "), level: LEVEL_WARN}, "", stdlog.Lshortfile)
		Error = stdlog.New(&logWriter{prefix: []byte("\x1b[01;31mERR\x1b[00m "), level: LEVEL_ERROR}, "", stdlog.Lshortfile)
		System = stdlog.New(&logWriter{prefix: []byte("\x1b[01;37mSYS\x1b[00m ")}, "", stdlog.Lshortfile)
	} else {
		Debug = stdlog.New(&logWriter{prefix: []byte("DEB "), level: LEVEL_DEBUG}, "", stdlog.Lshortfile)
		Info = stdlog.New(&logWriter{prefix: []byte("INF "), level: LEVEL_INFO}, "", stdlog.Lshortfile)
		Warn = stdlog.New(&logWriter{prefix: []byte("WAR "), level: LEVEL_WARN}, "", stdlog.Lshortfile)
		Error = stdlog.New(&logWriter{prefix: []byte("ERR "), level: LEVEL_ERROR}, "", stdlog.Lshortfile)
		System = stdlog.New(&logWriter{prefix: []byte("SYS ")}, "", stdlog.Lshortfile)
	}

	return nil
}

type logWriter struct {
	prefix []byte
	level  uint
}

func (w *logWriter) Write(b []byte) (int, error) {
	if logLevel < w.level {
		return 0, nil
	}

	row := append(w.prefix, []byte(time.Now().In(time.UTC).Format("2006-01-02T15:04:05.000Z"))...)
	row = append(append(row, ' '), b...)

	return fh.Write(row)
}
