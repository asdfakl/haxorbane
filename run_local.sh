#!/bin/bash

function die() {
	echo "$@" >&2
	exit 2
}

repo="$(realpath "$(dirname "${BASH_SOURCE}")")"

cd "$repo" || die "cd into $repo"

goos=$(go env GOOS)
goarch=$(go env GOARCH)

: ${goos:?'go env GOOS empty or undefined'}
: ${goarch:?'go env GOARCH empty or undefined'}

target="./target/${goos}/${goarch}/haxorbane"
cfg="./dev_config.toml"

test -x "$target" || die "missing executable: $target, make sure to first run ./build"
test -f "$cfg" || die "missing configuration file: $cfg, generate with $target -example-config > $cfg"

"$target" -c "$cfg"
