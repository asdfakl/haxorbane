package watcher

import (
	"bytes"
	"fmt"
	"io"
	"net"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"github.com/fsnotify/fsnotify"

	"gitlab.com/asdfakl/haxorbane/configuration"
	"gitlab.com/asdfakl/haxorbane/log"
)

type INotifyWatcher struct {
	cfg     *configuration.JailConfiguration
	watcher *fsnotify.Watcher
	fd      *os.File
	offset  int64
	buf     *bytes.Buffer

	once  sync.Once
	stopc chan struct{}
}

func NewINotify(cfg *configuration.JailConfiguration) Watcher {
	return &INotifyWatcher{
		cfg: cfg,
		buf: &bytes.Buffer{},

		stopc: make(chan struct{}, 1),
	}
}

func (w *INotifyWatcher) Start(eventc chan<- *Event) {
	w.once.Do(func() {
		go w.run(eventc)
	})
}

func (w *INotifyWatcher) Stop() {
	select {
	case w.stopc <- struct{}{}:
	default:
	}
}

func (w *INotifyWatcher) closeFSWatcher() {
	if w.watcher != nil {
		if err := w.watcher.Close(); err != nil {
			log.Warn.Printf("fs watcher close: %v", err)
		}
		w.watcher = nil
	}
}

func (w *INotifyWatcher) closeFD() {
	if w.fd != nil {
		if err := w.fd.Close(); err == nil {
			log.Debug.Printf("file descriptor closed")
		} else {
			log.Warn.Printf("close file descriptor: %v", err)
		}
		w.fd = nil
	}
	w.offset = 0
	w.buf.Truncate(0)
}

func (w *INotifyWatcher) watchSource(eventc chan<- *Event) {
	w.closeFD()

	info, err := os.Lstat(w.cfg.Source)
	if err != nil {
		if os.IsNotExist(err) {
			log.Info.Printf("lstat: %v", err)
		} else {
			log.Error.Printf("lstat: %v", err)
		}
		return
	}

	if !info.Mode().IsRegular() {
		eventc <- &Event{Error: fmt.Errorf("%s is not a regular file: %s", w.cfg.Source, info.Mode())}
		return
	}

	if w.watcher == nil {
		dir := filepath.Dir(w.cfg.Source)

		w.watcher, err = fsnotify.NewWatcher()
		if err != nil {
			log.Error.Printf("new fs watcher: %v", err)
			eventc <- &Event{Error: err}
			return
		}

		if err = w.watcher.Add(dir); err != nil {
			w.closeFSWatcher()
			eventc <- &Event{Error: fmt.Errorf("add fs watcher source %s: %v", dir, err)}
			return
		}
	}

	if w.fd, err = os.OpenFile(w.cfg.Source, os.O_RDONLY, 0644); err != nil {
		if os.IsNotExist(err) {
			log.Info.Printf("open file %s: %v", w.cfg.Source, err)
		} else {
			log.Warn.Printf("open file %s: %v", w.cfg.Source, err)
		}
	}

	log.Info.Printf("listening source %s", w.cfg.Source)
}

func (w *INotifyWatcher) run(eventc chan<- *Event) {
	defer close(eventc)
	defer w.closeFD()

	w.watchSource(eventc)
	if w.watcher == nil {
		return
	}

	ticker := time.NewTicker(2 * time.Second)
	defer ticker.Stop()

	refresh := true

	for {
		select {
		case <-w.stopc:
			w.closeFSWatcher()
			return
		case <-ticker.C:
			if w.fd == nil {
				w.watchSource(eventc)
				break
			}
			if refresh {
				refresh = w.readChanges(eventc)
			}
		case event := <-w.watcher.Events:
			if event.Name != w.cfg.Source {
				break
			}
			log.Debug.Printf("inotify event: %v", event)

			if event.Op&(fsnotify.Remove|fsnotify.Rename) != 0 {
				w.closeFD()
				break
			}
			if event.Op&fsnotify.Write != 0 {
				refresh = true
			}
		case err := <-w.watcher.Errors:
			eventc <- &Event{Error: err}
		}
	}
}

func (w *INotifyWatcher) readChanges(eventc chan<- *Event) (refresh bool) {
	info, err := w.fd.Stat()
	if err != nil {
		log.Error.Printf("stat source file %s: %v", w.cfg.Source, err)
		return
	}

	size := info.Size()

	if size == w.offset {
		// nothing to read
		return
	}
	if size == 0 || size < w.offset {
		// most likely truncated
		w.offset = 0
		w.buf.Truncate(0)
	}

	readSize := size - w.offset
	if readSize > MAX_READ_BYTES {
		readSize = MAX_READ_BYTES
		refresh = true
	}

	b := make([]byte, readSize)
	n, err := w.fd.ReadAt(b, w.offset)
	if err != nil && err != io.EOF {
		log.Error.Printf("read: %v", err)
		return
	}
	if n == 0 {
		return
	}
	w.offset += int64(n)
	w.buf.Write(b[0:n])

	var line []byte
	for {
		line, err = w.buf.ReadBytes('\n')
		if err != nil {
			if err == io.EOF {
				// incomplete line, put back to buffer
				w.buf.Write(line)
			} else {
				log.Error.Printf("buffer read line: %v", err)
			}
			break
		}
		line = bytes.TrimSpace(line)

		for _, rule := range w.cfg.Rules {
			if rule.FilterRE != nil && !rule.FilterRE.Match(line) {
				continue
			}

			ipMatch := w.cfg.IPRE.FindSubmatch(line)
			if len(ipMatch) < 2 {
				continue
			}
			ip := net.ParseIP(string(ipMatch[1]))
			if ip == nil {
				log.Warn.Printf("could not parse ip address from '%s' in rule %s", string(ipMatch[1]), rule.Name)
				continue
			}

			timeMatch := w.cfg.TimeRE.FindSubmatch(line)
			if len(timeMatch) < 2 {
				continue
			}

			t, err := time.Parse(w.cfg.TimeFormat, strings.Join(strings.Fields(string(timeMatch[1])), " "))
			if err != nil {
				log.Warn.Printf("could not parse time from '%s' in rule %s: %v", string(timeMatch[1]), rule.Name, err)
				continue
			}

			if t.Year() == 0 {
				t = time.Date(time.Now().Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second(), t.Nanosecond(), t.Location())
			}

			eventc <- &Event{
				IP:   ip,
				Time: t.In(time.UTC),
				Rule: rule.Name,
			}
		}
	}

	return
}
