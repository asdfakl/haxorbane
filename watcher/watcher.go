package watcher

import (
	"net"
	"time"
)

const (
	MAX_READ_BYTES = 1024 * 4096
)

type Watcher interface {
	Start(chan<- *Event)
	Stop()
}

type Event struct {
	Error error
	IP    net.IP
	Time  time.Time
	Rule  string
}
